public class Driver {
    private String name;
    private int age;
    private String address;

    /*
    *   Classes have relationships:
    *
    *   A Car could have a Driver. - "has relationship" - Composition - allows modeling objects to be made up of other objects.
    * */

    public Driver(){

    }

    public Driver(String name, int age, String address){
        this.name = name;
        this.age = age;
        this.address = address;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setAge(int age){
        this.age = age;
    }
    public void setAddress(String name){
        this.address = address;
    }

    public String getName(){
        return this.name;
    }

    public int getAge(){
        return this.age;
    }
    public String getAddress(){
        return this.address;
    }

}
