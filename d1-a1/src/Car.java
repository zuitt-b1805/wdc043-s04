public class Car {

    /*
    *   An object is an idea of a real-world object.
    *   A class is the code that describes the object.
    *   An instance is a tangible copy of an idea instantiated or created from a class.
    * */
//    Attributes and Methods
//    public - the variable in the class is accessible anywhere in the application.
//    Class attributes/properties must not be public, they should only be accessed and set via class methods called setters and getters.
//    private - limit the access and ability to set a variable/method to its class.
//    setters are public methods which will allow us to set the value of an attribute of an instance.
//    getters are public methods which will allow us to get the value of the attribute of an instance.

    private String make;
    private String brand;
    private int price;
//    Add a variable to add a property which is an object/instance of a Driver class.
    private Driver carDriver;

//    Methods are functions of an object that allows us to perform the tasks.
//    void - means that the function does not return anything. Because in Java, a function/methods' return dataType must be declared.
    public void start(){
        System.out.println("Vroom! Vroom!");
    }

//    parameters in Java needs its dataType declared.

/*    public void setMake(String makeParams){
//        "this" keyword refers to the object where the constructor or setter is.
        this.make = makeParams;
    }

//    make getter
    public String getMake(){
        return this.make;
    }

    public void setBrand(String brandParams){
        this.brand = brandParams;
    }

    public String getBrand(){
        return this.brand;
    }

    public void setPrice(int priceParams){
        this.price = priceParams;
    }

    public int getPrice(){
        return this.price;
    }*/

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getBrand() {
        return brand;
    }
//    To make a property read-only, don't include a setter function for that property.

/*    public void setBrand(String brand) {
        this.brand = brand;
    }*/

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
//    constructor is a method which allows us to set the initial values of an instance.
//    empty/default constructor - default constructor - allows us to create an instance with default initialized values for our properties
    /*public Car(){
//        it can be empty or you can designate default values instead of getting them from parameters.
//        Java, actually already creates one for us, empty/default constructor made just by Java allows us to Java to set its own default values. If you create your own, you can your own default values.
    }*/

//    parameterized constructor - allow us to initial values to our attribute upon creation of the instance

    /*public Car(String make, String brand, int price){
        this.make = make;
        this.brand = brand;
        this.price = price;
    }*/

    public Car() {
    }

    public Car(String make, String brand, int price, Driver driver) {
        this.make = make;
        this.brand = brand;
        this.price = price;
        this.carDriver = driver;
    }

    public Driver getCarDriver() {
        return carDriver;
    }

    public void setCarDriver(Driver carDriver) {
        this.carDriver = carDriver;
    }

    public String getCarDriverName(){
        return this.carDriver.getName();
    }
}
